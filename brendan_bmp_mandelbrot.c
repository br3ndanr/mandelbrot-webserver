//brendan's mandelbrot generator!
//8/4/2014
//Prints an image of the mandelbrot set
//mandelbrot plots within RE (-2, 2) and IM (2, 2) 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0

#define SIZE 512
#define BMP_FILE "mandelbrot.bmp"

#define RE_START -2
#define RE_STOP   2
#define IM_START  2
#define IM_STOP  -2
#define ITER_MAX 255

#define DELTA 0.05 

typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

typedef struct _complex {
    double re;
    double im;
} complex;

int isInMandelbrot(complex start);
void writeHeader (FILE *file);
void makeBMP();

int main(int argc, char *argv[]) {
    makeBMP();
    return 0;
}
void makeBMP() {
    FILE *outputFile;
    outputFile = fopen(BMP_FILE, "wb");
    assert ((outputFile!=NULL) && "Cannot open file");
    writeHeader(outputFile);

    int weight;
    int row = 0;
    int col = 0;
    
    complex delta = {(RE_STOP - RE_START)/(double)SIZE,(IM_START - IM_STOP)/(double)SIZE};
    complex current = {RE_START, IM_START};
    while (row < SIZE) {
        while (col < SIZE) {
            weight = isInMandelbrot(current);
            
            //printf("Cur: %f %fi, weight: %d\n", current.re, current.im, weight);

            fputc(ITER_MAX - weight, outputFile);
            fputc(ITER_MAX - weight, outputFile);
            fputc(ITER_MAX - weight, outputFile);
            current.re += delta.re;
            col++;

        }
        current.re = RE_START;
        col = 0;
        row++;
        current.im -= delta.im;
    }
    
    fclose(outputFile);
    return;
}

int isInMandelbrot(complex start) {
    complex next = {0,0};
    double temp;
    int i = 0;
    while (i < ITER_MAX && (next.re*next.re + next.im*next.im < 4)) {
        temp = next.re;
        next.re = next.re*next.re - next.im*next.im + start.re;
        next.im = 2*temp*next.im + start.im;
        i++;
    }

    return i;
}

void writeHeader (FILE *file) {
    assert(sizeof (bits8) == 1);
    assert(sizeof (bits16) == 2);
    assert(sizeof (bits32) == 4);

    bits16 magicNumber = MAGIC_NUMBER;
    fwrite (&magicNumber, sizeof magicNumber, 1, file);

    bits32 fileSize = OFFSET + (SIZE * SIZE * BYTES_PER_PIXEL);
    fwrite (&fileSize, sizeof fileSize, 1, file);

    bits32 reserved = 0;
    fwrite (&reserved, sizeof reserved, 1, file);

    bits32 offset = OFFSET;
    fwrite (&offset, sizeof offset, 1, file);

    bits32 dibHeaderSize = DIB_HEADER_SIZE;
    fwrite (&dibHeaderSize, sizeof dibHeaderSize, 1, file);

    bits32 width = SIZE;
    fwrite (&width, sizeof width, 1, file);

    bits32 height = SIZE;
    fwrite (&height, sizeof height, 1, file);

    bits16 planes = NUMBER_PLANES;
    fwrite (&planes, sizeof planes, 1, file);

    bits16 bitsPerPixel = BITS_PER_PIXEL;
    fwrite (&bitsPerPixel, sizeof bitsPerPixel, 1, file);

    bits32 compression = NO_COMPRESSION;
    fwrite (&compression, sizeof compression, 1, file);

    bits32 imageSize = (SIZE * SIZE * BYTES_PER_PIXEL);
    fwrite (&imageSize, sizeof imageSize, 1, file);

    bits32 hResolution = PIX_PER_METRE;
    fwrite (&hResolution, sizeof hResolution, 1, file);

    bits32 vResolution = PIX_PER_METRE;
    fwrite (&vResolution, sizeof vResolution, 1, file);

    bits32 numColors = NUM_COLORS;
    fwrite (&numColors, sizeof numColors, 1, file);

    bits32 importantColors = NUM_COLORS;
    fwrite (&importantColors, sizeof importantColors, 1, file);

}
