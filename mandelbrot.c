//mandelbrot.c
//Written by Brendan Roy and Adam Dobrzycki
//Created 8/4/2014
//This program forms the bulk of the 1917 task 2
//assignment. It handles incomming http requests
//and returns either:
//"/" An index page linking the almondbread 
//  mandelbrot webpage
//"/tile_x-X.X_yX.X_zX.bmp" (where X denotes an 
//  integer) A bitmap image generated with center
//  and zoom factor denoted by x y and z values.
//"/anything_else" a nice 404 page (Not in 
//  assignment spec)


#include "mandelbrot.h"
#include "pixelColor.h"

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <unistd.h> 

#define SIMPLE_SERVER_VERSION 1.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 1917

#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0

#define SIZE 512
#define ITER_MAX 256
#define BUFFER_LEN 100

//Set DEBUG to 0 to disable debugging output
//Set to 1 to enable.
#define DEBUG 0

typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

typedef struct _complex {
    double re;
    double im;
} complex;

int waitForConnection (int serverSocket);
int makeServerSocket (int portno);
void serveIndex(int socket);
void serveMandelbrot(int socket, complex coord, int zoom);
void serve404(int socket);
void handle(int socket, char* request);
void writeHeader (int socket);

//Main funcion serves as our connection loop. It accepts connections
//from a client, handles them and closes them nicely.
int main(int argc, char *argv[]) {
    printf("[INFO] Initalising Server on port %d.\n",DEFAULT_PORT);

    //create server socket which will accept incomming connections
    int serverSocket = makeServerSocket (DEFAULT_PORT);
    
    //declare array to hold the user request.
    char request[REQUEST_BUFFER_SIZE];
    
    //We create a readCount so the server won't run FOREVER 
    //and we can keep track of the number of clients we've served.
    int readCount = 0;
    while(readCount < 100) {
        //blocking call which waits for a connection from a client.
        //Then creates a new socket and returns it.
        int connectionSocket = waitForConnection (serverSocket);

        //Read the client's request.
        int bytesRead;
        bytesRead = read(connectionSocket, request, 
                (sizeof request)-1);
        assert (bytesRead >= 0); 
        // were we able to read any data from the connection?

        if (DEBUG) {
            printf("[DBUG] Connection established on: %d\n", 
                    connectionSocket);
        }
        //Now we call our handle function, which will parse the 
        //request and call the appropriate response function. Once 
        //this function completes,we will close the connection.
        handle(connectionSocket,request);

        // close the connection after sending the page
        close(connectionSocket);
        if (DEBUG) {
            printf("[DBUG] Connection closed\n");
        }

        readCount++;
        printf ("[INFO] *** So far served %d complete pages "
                "***\n\n", readCount);
    }

    return EXIT_SUCCESS;
}


//Called in main, handles a web request
//This function parses the request,
//and decides what to do based on the request data.
void handle(int socket, char* request){
    //Initalise a buffer to hold the data we interpret.
    //The last element should stay zero, so we can treat our
    //buffer as a string.
    char buff[BUFFER_LEN] = {0};

    //We need to know where we are while parsing.
    int index = 0;

    //we only want to serve replies to GET requests. (NOTE:
    //while this wasn't in assignment spec I thought it added
    //security and clarity to our solution. The assignment doesn't
    //ask at all about this kind of filtering, but I think it is
    //a good addition.)
    if (request[0] == 'G' && request[1] == 'E' &&  request[2] == 'T') {
        //Now we copy the only part of the request we care about,
        //the URL, into our buffer for later processing.
        while (index < (BUFFER_LEN -1) && request[index + 4] != ' ') {
            //request is +4 because we don't want " GET " in our URL.
            //BUFFER_LEN - 1 ensures the last char in buff is null.
            buff[index] = request[index+4];
            index++;
        }
        //Just making sure that our buffer can still be a string (and
        //that we didn't overflow.)
        assert(strlen(buff) < BUFFER_LEN);
        //We now have our URL. Let's print it for debugging and/or
        //logging purposes.
        printf("[INFO] GET %s\n", buff);

        //If request is "/"
        //Note that this is obviously not going to filter requests
        //like "GET / IM A BUS"
        if (buff[1] == 0) {
            serveIndex(socket);
        } 

        //else not asking for index page. According to the assignment
        //spec this means they want a tile. Since we're good people
        //we're also going to check that they want a tile... if they
        //don't then give a friendly 404 page pointing them in
        //the right direction :)
        else {
            //if they want a /tile[blah]
            if (buff[1] == 't' && buff[2] == 'i' &&
                    buff[3] == 'l' && buff[4] == 'e'){
                //now we have a request that we assume looks like this:
                //"/tile_xX.X_yX.X_zX.bmp"
                //in regex terms: *.?_x(.*?)_y(*.?)_z(*.?).bmp

                //Allocate memory for the variables. Better to do it 
                //here than at the start of the function, this way
                //we don't waste time/memory if we're going to serve
                //a 404 or the index page. We have to assign values
                //to them because otherwise the compiler cries.
                complex coords = {0,0};
                int zoom = 0;

                //now loop through the request, looking for '_'
                //When we find '_', convert the number after it
                //based on the letter before it. We know the number
                //has ended when we hit another '_'. Since the zoom 
                //doesn't end with an underscore, we're going to
                //loop again for it. We also need a temp index to 
                //tell us when the number started once we find its
                //end. Now we have those things, stdlib atof() will
                //take care of the rest, INCLUDING any negative signs
                //we might encounter (yewwww, stdlib!). The last thing
                //we need is a second buffer which we can add our 
                //number to as we add more to it.
                int i,j; //Loop variables
                int temp = 0;
                //We're using BUFFER_LEN again for simplicity: we know
                //that it will be a standard length when we check if 
                //we're overflowing it.
                char numberBuff[BUFFER_LEN] = {0};

                i = 0;
                //Loop through and get the x and y values of the 
                //request.
                while (i < sizeof(buff)) {
                    //Underscores denote the beginning (and end!)
                    //of a number. Thus we need to perform special
                    //operations when we hit one.
                    if (buff[i] == '_'){
                        //Temp is only zero at the first underscore.
                        //This means we want to start writing the 
                        //following data into our buffer.
                        if (temp == 0) {
                            temp = i+1;
                        } 
                        //else our temp wasn't zero so this means
                        //we're at the end of either the x or y 
                        //number.
                        else {
                            //if it was x: 
                            if (buff[temp] == 'x'){
                                //Convert string to float
                                coords.re = atof(numberBuff);
                                if (DEBUG) {
                                    printf("[DBUG] Real: %f is "
                                            "atoi of %s\n",
                                            coords.re,numberBuff);
                                }
                                //Reset our buffer.
                                //since memset is forbidden, loop to
                                //reset the numberBuff.
                                j = 0;
                                while (j < sizeof(numberBuff)) {
                                    numberBuff[j] = 0;
                                    j++;
                                }
                                //Because we hit underscore for x
                                //we're at the start of the y number.
                                //We have to set temp to that.
                                temp = i+1;
                            }
                            //else it was y
                            else if (buff[temp] == 'y'){
                                //make float
                                coords.im = atof(numberBuff);
                                if (DEBUG) {
                                    printf("[DBUG] Imag: %f is "
                                            "atoi of %s\n",
                                            coords.im,numberBuff);
                                }
                                //reset buffer.
                                j = 0;
                                while (j < sizeof(numberBuff)) {
                                    numberBuff[j] = 0;
                                    j++;
                                }
                                //change temp so we don't go 
                                //overwriting our now CORRECT
                                //y value.
                                temp = i+1;
                            }
                        }
                    } 
                    //We didn't hit an underscore. So, if temp is not 
                    //zero (it denotes the index of either 'x' or 'y')
                    //we need to record a number into our buffer.
                    //Unless, of course, we're actually AT temp,
                    //since 'x' and 'y' aren't numbers. Note that
                    //we will be storing decimal places in our buffer,
                    //but atof handles those.
                    else if (temp != 0 && i != temp) {
                        //Check that we're not smashing the stack
                        //here.
                        assert((i-temp-1) < (BUFFER_LEN -1));
                        //We're not? Cool! Record that number!
                        numberBuff[i-temp-1] = buff[i];
                    }
                    //Increase index of loop.
                    i++;
                }

                //do it again for z
                //Need to clear our numberBuff
                j = 0;
                while (j < sizeof(numberBuff)) {
                    numberBuff[j] = 0;
                    j++;
                }
                
                i = 0;
                temp = 0;
                //while we're in range of the buffer and
                //we still want to loop
                while (i < sizeof(buff) && temp != -1) {
                    //We hit the start of z number
                    if (buff[i] == 'z') {
                        //set temp so we start recording.
                        temp = i;
                    } 
                    //We hit a stop. This either happens in
                    //x or y, or at '.bmp'. Since the z
                    //number comes last, we only do this
                    //if we've passed 'z' (temp is not zero)
                    else if (buff[i] == '.' && temp != 0){
                        //Make that integer!
                        zoom = atoi(numberBuff);
                        if (DEBUG) {
                            printf("[DBUG] Zoom: %d is atoi of %s\n"
                                    ,zoom,numberBuff);
                        }
                        //stop looping. We found our number.
                        temp = -1;
                    } 
                    //Temp isn't zero so we want to record
                    else if (temp != 0) {
                        //We don't like buffer overflows.
                        assert((i-temp-1) < (BUFFER_LEN - 1));
                        //Record the number
                        numberBuff[i-temp-1] = buff[i];
                    }
                    i++;
                }

                //At this point we have all we need to make 
                //a mandelbrot. So we make it!
                serveMandelbrot(socket,coords, zoom);
                //After we're done, there's nothing left to do.
                //Next stop: return; !
            }
            //The request isn't for a tile, and it isn't for the index
            //so serve a 404. Like the request filtering, while this
            //wasn't in the assignment spec it adds to the clarity
            //of the server, and also allowed for debugging of 
            //the parser.
            else {
                //Dump debugging details
                if (DEBUG) {
                    printf("[DBUG] 404 reached. \n %s\n", request);
                    printf("[DBUG] ===Detail===\n");
                    printf("[DBUG] buff:\n%s\n",buff);
                }
                //Serve the 404 page. Like for serveMandelbrot, once
                //this is done we're going to return to main.
                serve404(socket);
            }
        }

    } 
    //We didn't get a GET request! Oh no! Dump detail because
    //this shouldn't be happening. Don't respond though.
    else {
        if (DEBUG) {
            printf("[ERR!] Bad request recieved:\n %s\n", request);
            printf("===Detail===\n");
            printf("Request type: [%c%c%c]\n", 
                    request[0], request[1], request[2]);
        }
    }
    //Back to main we go!
    return;
}

//serves a mandelbrot bmp image to a given socket. This 
//image is generated based off the coordinates and zoom
//level given.
void serveMandelbrot (int socket, complex coord, int zoom) {
    if (DEBUG) {
        printf("[DBUG] serving mandelbrot at x:%f y:%f z:%d\n",
                coord.re, coord.im, zoom);
    }   
    //Make a sting var to hold our HTTP response header.
    char* message;
    // first send the http response header

    // (if you write stings one after another like this on separate
    // lines the c compiler kindly joins them togther for you into
    // one long string)
    message = "HTTP/1.0 200 OK\r\n"
        "Content-Type: image/bmp\r\n"
        "\r\n";
    //We check that write completed sucessfully for multiple reasons.
    //ONE because it's important to do so, but 2 because the compiler
    //might cry if we don't and >portability is good.
    if (write (socket, message, strlen (message)) < strlen (message)) {
        printf("[ERR!] write failed to send all bytes of HTTP header");
    }
    // now create and send the BMP header. We're actually going to do 
    // this by pushing bytes down the pipe, for each subsection of the
    // header. This is handy, because it means we don't have to waste
    // resources on our system creating and storing this info.
    if (DEBUG) {
        printf("[DBUG] Writing header...");
    }
    writeHeader(socket);
    if (DEBUG){
        printf(" done.\n");
    }
    // now send the BMP. First allocate an array for the pixels.
    // Remember that there are (usually) 3 bytes per pixel.
    //unsigned char bmp_data[SIZE*BYTES_PER_PIXEL*SIZE];
    //
    // now generate said BMP by populating our array with the 
    // appropriate value for each mandelbrot pixel.
    int weight; //This will hold the result of escapeSteps.
    int row = 0; //Rows and cols to iterate through.
    int col = 0;
    
    //Will hold the RGB values before we write them
    int red, green, blue;

    //our delta value is the distance between pixels.
    //it equals 2**-z.
    double delta = 1;
    if (DEBUG) {
        printf("[DBUG] Creating delta...");
    }
    //If delta is greater than zero, divide by 2.
    //If it's less than zero, multiply by 2! 
    //This way we handle (_z-5), if we ever needed to, which 
    //we quite possibly might. Once again, this isn't in
    //assignment spec but I feel it is good craftsmanship
    //and an important thing to include.
    while (zoom != 0) {
        if (zoom > 0) {
            delta /= 2;
            zoom--;
        } else {
            delta *= 2;
            zoom++;
        }
    }
    //Make sure we did the last step right.
    assert(zoom == 0);
    if (DEBUG) {
        printf(" done.\n");
    }

    //Now we define the starting point of our calculations.
    //Remembering that BMP pixel arrays are defined from the
    //bottom left instead of the top left, we start our x and
    //y values both in the 'negative' and then add delta to them
    //as we progress.
    complex current = {coord.re-((SIZE/2)*delta), 
                       coord.im-((SIZE/2)*delta)};
    if (DEBUG) {
        printf("[DBUG] Connection: %d\n", socket);
    }
    if (DEBUG) {
        printf("[DBUG] Current:%f %f\n", current.re, current.im); 
    }
    //For every row in the BMP
    while (row < SIZE) {
        //and for every column in that row,
        while (col < SIZE) {
            //Get the weight of the corresponding pixel.
            //We do ITER_MAX - escape steps to make our mandelbrot
            //black.
            weight = ITER_MAX - escapeSteps(current.re, current.im);
            //Make RGB values!
            red = stepsToRed(weight);
            green = stepsToGreen(weight);
            blue = stepsToBlue(weight);

            //Write each byte. Czech that we're doing it properly!
            if (write (socket, &red, 1) < 1 ||
                write (socket, &green, 1) < 1 ||
                write (socket, &blue, 1) < 1 ){

                printf("[ERR!] Failed to write bmp pixel at"
                        " (%i, %i)\n", row, col);
            }
            //Add our delta value to our column value.
            //Increment col too so it represents the new pixel.
            current.re += delta;
            col++;

        }
        //Reset col and our column value as we're at the start
        //of a new row.
        current.re = coord.re-((SIZE/2)*delta);
        col = 0;
        //Update row and our row value as we're at the start
        //of a new row.
        row++;
        current.im += delta;
    }

    //We're done! Yay :) We let main close the socket.
    if (DEBUG) {
        printf("[DBUG] BMP generated and sent.\n");
    }
    //back to handle!
    return;
}

//This function determines how many steps a coordinate takes
//to escape the mandelbrot set. It returns that value.
int escapeSteps(double x, double y) {
    //Initalise necessary variables.
    complex next = {0,0};
    double temp;
    int i = 0;
    //While we're still in the mandelbrot set and we're not going over
    //our iteration maximum, perform the next step.
    while (i < ITER_MAX && (next.re*next.re + next.im*next.im < 4)) {
         //We have to make a temp value so we don't mess up calcuating
         //the imaginary part.
        temp = next.re;
        next.re = next.re*next.re - next.im*next.im + x;
        next.im = 2*temp*next.im + y;
        i++;
    }

    return i;
}

//serves the almondbread webpage!
void serveIndex(int socket) {
    if (DEBUG) {
        printf("[DBUG] serving index page\n");
    }
    //make our response
    const char* message = "HTTP/1.0 200 OK\r\n"
        "Content-Type: text/html\r\n"
        "\r\n"
        "<!DOCTYPE html>\n"
        "<script src=\"http://almondbread.cse.unsw.edu.au/tiles.js\">"
        "</script>\n";
    
    //Remember we want to make sure things are working.
    if (write (socket, message, strlen (message)) < strlen (message)) {
        printf("[ERR!] write failed to send all bytes of index page");
    }
    return;
}

//serves a beautiful 404 page
void serve404(int socket) {
    if (DEBUG) {
        printf("[DBUG] serving 404 page\n");
    }   
    //Make message
    const char* message = "HTTP/1.0 200 OK\r\n"
        "Content-Type: text/html\r\n"
        "\r\n"
        "<!DOCTYPE html>\n"
        "<head><title>404 - Page not found</title></head>\n"
        "<body><h1>404</h1><div>"
        "The page you requested does not exist.</div>"
        "<div>Try navigating to the index or "
        "/tile_x-X.X_yX.X_zX.bmp</div>"
        "</body>";
    //send and check message.
    if (write (socket, message, strlen (message)) < strlen (message)) {
        printf("[ERR!] write failed to send all bytes of 404 page ");
    }
    return;
}
//This function creates and returns a serverSocket which listens
//on the given port for connections.
int makeServerSocket (int portNumber) { 
    // create socket
    int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
    assert (serverSocket >= 0);   
    // error opening socket

    // bind socket to listening port
    struct sockaddr_in serverAddress;
    //bzero doesn't work in C99. Keep this line handy for if we 
    //ever need to compile to a more recent standard.
    //memset ((char *) &serverAddress,0, sizeof (serverAddress));
    bzero ((char *) &serverAddress, sizeof (serverAddress));

    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons (portNumber);

    // let the server start immediately after a previous shutdown
    int optionValue = 1;
    setsockopt (
            serverSocket,
            SOL_SOCKET,
            SO_REUSEADDR,
            &optionValue, 
            sizeof(int)
            );

    int bindSuccess = 
        bind (
                serverSocket, 
                (struct sockaddr *) &serverAddress,
                sizeof (serverAddress)
             );

    assert (bindSuccess >= 0);
    // if this assert fails wait a short while to let the operating 
    // system clear the port before trying again

    return serverSocket;
}

// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
int waitForConnection (int serverSocket) {
    // listen for a connection
    const int serverMaxBacklog = 10;
    listen (serverSocket, serverMaxBacklog);

    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    int connectionSocket = 
        accept (
                serverSocket, 
                (struct sockaddr *) &clientAddress, 
                &clientLen
               );

    assert (connectionSocket >= 0);
    // error on accept

    return (connectionSocket);
}

//Writes the BMP header to the given socket.
void writeHeader (int socket) {
    //We rely on these types. If the OS we are trying to run on 
    //has defined these differently, then the program will not work.
    assert(sizeof(bits8) == 1);
    assert(sizeof(bits16) == 2);
    assert(sizeof(bits32) == 4);

    bits16 magicNumber = MAGIC_NUMBER;
    if (write(socket, &magicNumber, sizeof (magicNumber)) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 fileSize = OFFSET + (SIZE * SIZE * BYTES_PER_PIXEL);
    if (write(socket, &fileSize, sizeof fileSize) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 reserved = 0;
    if (write(socket, &reserved, sizeof reserved) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 offset = OFFSET;
    if (write(socket, &offset, sizeof offset) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 dibHeaderSize = DIB_HEADER_SIZE;
    if (write(socket, &dibHeaderSize, sizeof dibHeaderSize) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 width = SIZE;
    if (write(socket, &width, sizeof width) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 height = SIZE;
    if (write(socket, &height, sizeof height) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits16 planes = NUMBER_PLANES;
    if (write(socket, &planes, sizeof planes) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits16 bitsPerPixel = BITS_PER_PIXEL;
    if (write(socket, &bitsPerPixel, sizeof bitsPerPixel) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 compression = NO_COMPRESSION;
    if (write(socket, &compression, sizeof compression) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 imageSize = (SIZE * SIZE * BYTES_PER_PIXEL);
    if (write(socket, &imageSize, sizeof imageSize) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 hResolution = PIX_PER_METRE;
    if (write(socket, &hResolution, sizeof hResolution) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 vResolution = PIX_PER_METRE;
    if (write(socket, &vResolution, sizeof vResolution) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 numColors = NUM_COLORS;
    if (write(socket, &numColors, sizeof numColors) < 0) {
        printf("Error writing bmp header\n");
    } 

    bits32 importantColors = NUM_COLORS;
    if (write(socket, &importantColors, sizeof importantColors) < 0) {
        printf("Error writing bmp header\n");
    } 
    return;
}
