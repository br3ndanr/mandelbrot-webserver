//brendan's mandelbrot generator!
//8/4/2014
//Prints an image of the mandelbrot set
//mandelbrot plots within RE (-2, 2) and IM (2, 2) 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ITER_MAX 255
#define DELTA 0.05

typedef struct _complex {
    double re;
    double im;
} complex;

int isInMandelbrot(complex start);

int main() {
    complex current = {-2, 2};
    while (current.im >=-2) {
        while (current.re <= 2) {
            if (isInMandelbrot(current) == ITER_MAX) {
                printf("#");
            } else {
                printf(" ");
            }
            current.re += DELTA;
        }
        current.re = -2;
        printf("\n");
        current.im -= DELTA;
    }

    return 0;
}

int isInMandelbrot(complex start) {
    complex next = {0,0};
    double temp;

    int i = 0;
    while (i < ITER_MAX && (next.re*next.re + next.im*next.im < 4)) {
        temp = next.re;
        next.re = next.re*next.re - next.im*next.im + start.re;
        next.im = 2*temp*next.im + start.im;
        i++;
    }

    return i;
}
