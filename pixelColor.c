//pixelColor.c
//Written by Brendan Roy
//17/4/2014
//This file implements the 3 coloring functions for mandelbrot.c
#include "pixelColor.h"
#define ITER_MAX 256

unsigned char stepsToRed (int steps){
    return ITER_MAX - steps;
}

unsigned char stepsToBlue (int steps){
    return steps;
}

unsigned char stepsToGreen (int steps){
    return 0;
}

