Mandelbrot Webserver

Written by Brendan Roy and Adam Dobrzycki

USAGE:

$ gcc mandelbrot.c pixelcolor.c -o runServer
$ ./runServer

This program serves mandelbrot tiles to users.
It is unthreaded, meaning that the server can
only maintain one connection at a time.
